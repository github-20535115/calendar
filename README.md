# calendar
原生js日历插件
最近因为项目需求，懒的看别人的源码所以自己动手写了一个原生的js日历插件，
这里就简单分析一下， js，css，html都在calendar.html里
### 需要了解

```
var date=new Date();
//获取
date.getFullYear();//获取年份（2018）
date.getMonth();//获取月份（number类型 ，1）
date.getDate();//获取日期(21)
date.getDay();//获取星期（3）
//设置
date.setFullYear(2019);
date.setMonth(1);
date.setDate(21);
date.setDay(3);
```

### 需要注意

 1. 

今天是2018年2月21日星期三，看上面的代码可以发现月份是1,  这里需要注意月份是从0开始算的。

 2. 2018年2月0日，即2018年1月31日，这在渲染每个月的具体天数时有用，可以获取每个月最大天数
 
### 截图

![输入图片说明](https://gitee.com/uploads/images/2018/0221/142250_8c289304_1536541.png "图片1.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0221/142302_c0799357_1536541.png "图片2.png")


### 代码详解
 
  

如何用

传进去id，点击这个id所属的div（或button、span等），就会出现日历，选择的日期就会显示为这个div的innerHTML。如下代码
 

```
        calendar.init('date1');
        calendar.init('date2');
```


细数函数

 1. init(para1)
		 获取传进来的id所属的对象，给此对象添加点击事件
 2. addEvent()
		 执行点击事件，
 3. renderHTML()
			 点击后执行此函数，用来显示日历，默认当前日期
			 ，HTML渲染完毕之后，开始获取需要的节点，并
			 为按钮添加点击事件（上一月，下一月，上一年，
			 下一年，关闭日历）
 4. closeCalendar()
	 关闭日历，即是移除节点的操作，移除第三个函数产生
	 的div
 5. nextYear()
	 年数+1，并设置month，date，为该年第一个月第一
	 天，后设置month+1，date为2（例0+1即为2月，date为
	 0，即是2月0日，显然不合理，js就会把它转化为1月的
	 最后一天，前面有提到过~~~），便获取到该年第一个
	 月 一共有多少天，有了这个数据便可以渲染这一年这一
	 月的日历详情了
 6. nextMonth()
		原理同上，不过年数不变，月份+2，date设为0，便可
		以获取到下个月的最大天数，
 7. preYear()
 8. preMonth()
	 上一月，上一年的情况就不再赘述~~~，情况差不多
 9. renderCalendar()
	 此函数就是根据每个月的最大天数来循环产生每一天的
	 span，为他添加click事件，并设置margin-left属性
 10. choseDate()
	 选中该月的这个日期，便改变innerHTML
 11. switchDay()
	 将用数字表示的星期，改为用汉字


本人小白~如果有哪里不对的还望各位看官指正 ^_^

 